FROM python:3.6.6
LABEL maintainer="gsarantid@gmail.com"

ENV SECRET_KEY ddc06a8e8b9d048f31c3cf630a8ff01c

COPY  . /app
WORKDIR /app

RUN pip3.6 install -r requirements.txt

ENTRYPOINT ["python3"]
CMD ["run.py"]
