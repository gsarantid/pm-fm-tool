from web_generator import create_app

app = create_app()

if __name__ == '__main__':
    app.config['WTF_CSRF_ENABLED']= False
    app.run(host='0.0.0.0', port=8085)
