import os
import json
from flask import (
    render_template,
    url_for,
    flash,
    redirect,
    request,
    abort,
    send_file,
    current_app,
    Blueprint
)
from web_generator.alarms.forms import AlarmForm
from web_generator.tools.parse_probable_cause import get_probable_cause_choices, get_probable_cause_index

alarms = Blueprint('alarms', __name__)

UNIX_NEWLINE = '\n'
WINDOWS_NEWLINE = '\r\n'


@alarms.route("/alarm/new", methods=['GET', 'POST'])
def new_alarm():
    form = AlarmForm()
    form.probable_cause.choices = get_probable_cause_choices()
    form.probable_cause_index.data = get_probable_cause_index(form.probable_cause.data)

    if request.method == 'POST':
        if form.validate_on_submit():
            form_validations(form)
            new_alarm = render_template('alarm_definition.html', form=form)

            # Replace windows newline characters
            new_alarm = new_alarm.replace(WINDOWS_NEWLINE, UNIX_NEWLINE)

            filename = 'ald_{}_v_{}_{}.xml'.format(form.alarm_name.data,
                                                   form.edition.data,
                                                   form.revision.data)
            file_path = os.path.join(current_app.root_path, 'tools/new_alarms/', filename)
            os.makedirs(os.path.dirname(file_path), exist_ok=True)
            with open(file_path, 'w') as fh:
                fh.write(new_alarm)
            return send_file(
                file_path,
                mimetype='text/xml',
                attachment_filename=filename,
                as_attachment=True,
            )
        else:
            flash('Validation failed!', 'danger')
    return render_template('create_alarm.html', title='New Alarm', form=form, legend='New Alarm')


def form_validations(form):
    if form.edition.data is None:
        form.edition.data = 1
    if form.revision.data is None:
        form.revision.data = 0
