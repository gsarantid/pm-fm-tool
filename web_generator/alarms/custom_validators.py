from wtforms.validators import ValidationError


HEADER_FIELDS = {'type', 'level', 'facility', 'time', 'process',
                 'service', 'system', 'neid', 'container', 'host', 'timezone'}


def validate_alarm_field_name():
    message = """
        Must be different from Header-defined fields i.e
        type, level, facility, time, process, service, system, neid, container, host, timezone    
    """

    def _validate_alarm_field_name(form, field):
        if field.data in HEADER_FIELDS:
            raise ValidationError(message)

    return _validate_alarm_field_name


''' FC000925.08: It should enforce that TTL equals to zero if warning severity is selected '''
def validate_alive_time_value():
    message = '"Warning" severity cannot be associated with non-zero alive time'

    def _validate_alive_time_value(form, field):
        if form.severity.data == 'warning 5' and form.alive_time.data == 'SEC_TILL_CANCEL' and field.data != 0:
            raise ValidationError(message)

    return _validate_alive_time_value
