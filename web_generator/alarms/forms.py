import re
from flask_wtf import FlaskForm
from wtforms import (
    StringField, SubmitField, TextAreaField, IntegerField,
    SelectField, FieldList, FormField, BooleanField, SelectMultipleField
)
from wtforms.widgets import ListWidget, CheckboxInput
from wtforms.validators import DataRequired, Regexp, Length, NumberRange, Optional, ValidationError
from web_generator.alarms.custom_validators import (
    validate_alarm_field_name,
    validate_alive_time_value
)


class MultiCheckboxField(SelectMultipleField):
	widget = ListWidget(prefix_label=False)
	option_widget = CheckboxInput()


class AlarmFieldForm(FlaskForm):
    alarm_field_name = StringField(
        'Field name',
        validators=[
            Optional(),
            Regexp('^(([a-z0-9]+)(_[a-z0-9]+)*)$',
                   message='Alarm field name must use the following characters: a-z | 0-9 | _'),
            validate_alarm_field_name()
        ],
        render_kw={'placeholder': 'Field name'}
    )
    alarm_field_description = TextAreaField(
        'Field description',
        validators=[
            Optional()
        ],
        render_kw={'placeholder': 'Field description'}
    )
    alarm_field_type = StringField(
        'Field type',
        validators=[
            Optional(),
            Regexp('^(([a-z0-9/]+)(_[a-z0-9/]+)*)$',
                   message='Alarm field type must use the following characters: a-z | 0-9 | _ | /')
        ],
        render_kw={'placeholder': 'Field type'}
    )


class CompareFieldForm(FlaskForm):
    is_comparable = BooleanField(
        'Comparable'
    )

    compare_field_name = StringField(
        'Field name',
        validators=[
            Optional(),
            Regexp('^(([a-z0-9]+)(_[a-z0-9]+)*)$',
                   message='Compare field name must use the following characters: a-z | 0-9 | _')
        ]
    )
    compare_field_description = TextAreaField(
        'Field description',
        validators=[
            Optional()
        ]
    )
    compare_field_type = StringField(
        'Field type',
        validators=[
            Optional(),
            Regexp('^(([a-z0-9/]+)(_[a-z0-9/]+)*)$',
                   message='Compare field type must use the following characters: a-z | 0-9 | _ | /')
        ]
    )


class HeaderFieldForm(FlaskForm):
    is_comparable = BooleanField(
        'Comparable'
    )

    header_field_name = StringField(
        'Field name',
        validators=[
            Optional(),
            Regexp('^(([a-z0-9]+)(_[a-z0-9]+)*)$',
                   message='Header field name must use the following characters: a-z | 0-9 | _')
        ]
    )
    header_field_description = TextAreaField(
        'Field description',
        validators=[
            Optional()
        ]
    )
    header_field_type = StringField(
        'Field type',
        validators=[
            Optional(),
            Regexp('^(([a-z0-9/]+)(_[a-z0-9/]+)*)$',
                   message='Header field type must use the following characters: a-z | 0-9 | _ | /')
        ]
    )


class AlarmForm(FlaskForm):
    alarm_name = StringField(
        'Alarm name',
        validators=[
            DataRequired(),
            Regexp(r'^(([a-z0-9]+)(_[a-z0-9]+)*)$',
                message='Alarm name must use the following characters: a-z | 0-9 | _')
        ]
    )

    alarm_id = IntegerField(
        'Alarm ID',
        validators=[
            DataRequired(),
            NumberRange(min=160001, max=164000, message='Valid range: 160001-164000')
        ]
    )

    legacy_id = IntegerField(
        'Legacy ID',
        validators=[
            Optional(),
            NumberRange(min=1, max=25000, message='Valid range: 1-25000')
        ]
    )

    alarm_text = StringField(
        'Alarm text',
        validators=[
            DataRequired(),
            Regexp(r'^(([a-z0-9.\-/]+)( [a-z0-9.\-/]+)*)$',
                message='Alarm text must use the following characters: a-z | 0-9 | . | - | /  and single space)'),
            Length(min=1, max=80, message='Up to 80 characters allowed')
        ]
    )

    meaning = TextAreaField(
        'Meaning',
        validators=[
            DataRequired()
        ]
    )

    effect = TextAreaField(
        'Effect'
    )

    instructions = TextAreaField(
        'Instructions',
        validators=[
            DataRequired()
        ]
    )

    testing_procedure = TextAreaField(
        'Testing procedure',
        validators=[
            DataRequired()
        ]
    )

    canceling = TextAreaField(
        'Canceling',
        validators=[
            DataRequired()
        ]
    )

    severity = SelectField(
        'Severity',
        choices=[
            ('indeterminate 0', 'Indeterminate'),
            ('critical 2', 'Critical'),
            ('major 3', 'Major'),
            ('minor 4', 'Minor'),
            ('warning 5', 'Warning'),
            ('critical major minor warning 7', 'Critical Major Minor Warning'),
            ('critical major minor 8', 'Critical Major Minor'),
            ('critical major 9', 'Critical Major'),
            ('major minor warning 11', 'Major Minor Warning'),
            ('critical major warning 12', 'Critical Major Warning'),
            ('major minor 13', 'Major Minor'),
            ('minor warning 14', 'Minor Warning'),
            ('critical warning 15', 'Critical Warning')
        ]
    )

    alive_time = SelectField(
        'Alive time',
        choices=[
            ('ALIVE_TILL_CANCEL', 'Alive until canceled'),
            ('DEF_ALIVE_TIME_OF_DISTURBANCE', 'Default alive time of a disturbance'),
            ('SEC_TILL_CANCEL', 'Alive time in seconds'),
        ]
    )

    alive_time_value = IntegerField(
        'Alive time (seconds)',
        validators=[
            Optional(),
            NumberRange(min=0, max=4294967295, message='Valid range: 0-4294967295 (uint32)'),
            validate_alive_time_value()
        ]
    )

    is_threshold_alarm = SelectField(
        'Threshold alarm',
        choices=[
            ('false', 'False'),
            ('true', 'True')
        ]
    )

    requires_cancelation = SelectField(
        'Requires cancelation',
        choices=[
            ('false', 'False'),
            ('true', 'True')
        ]
    )

    event_type = SelectField(
        'Event type',
        choices=[
            (None, '--Select--'),
            ('communicationsAlarm', 'Communications Alarm'),
            ('equipmentAlarm', 'Equipment Alarm'),
            ('environmentalAlarm', 'Environmental Alarm'),
            ('integrityViolation', 'Integrity Violation'),
            ('operationalViolation', 'Operational Violation'),
            ('physicalViolation', 'Physical Violation'),
            ('processingErrorAlarm', 'Processing Error Alarm'),
            ('qualityOfServiceAlarm', 'Quality of Service Alarm'),
            ('securityServiceOrMechanismViolation', 'Security Service or Mechanism Violation'),
            ('timeDomainViolation', 'Time Domain Violation')
        ]
    )
    probable_cause = SelectField(
        'Probable cause',
        choices=[]
    )

    probable_cause_index = IntegerField(
        'Probable cause index',
    )

    alarm_fields = FieldList(FormField(AlarmFieldForm), min_entries=1) # , validators=[Optional()])

    compare_fields = SelectField(
        'Compared fields',
        choices=[
            ('DEFAULT', 'Default'),
            ('LIST', 'Choose from list'),
            ('ALL', 'All alarm fields')
        ],
        default='DEFAULT'
    )

    compare_fields_list = FieldList(FormField(CompareFieldForm), min_entries=0)

    header_fields_list = FieldList(FormField(HeaderFieldForm), min_entries=0)

    product = StringField(
        'Product',
        default='My Product'
    )

    product_release = SelectField(
        'Product release',
        choices=[
            (None, '--Select--'),
            ('RELEASE_1', 'RELEASE_1'),
            ('RELEASE_2', 'RELEASE_2'),
            ('RELEASE_3', 'RELEASE_3')
        ]
    )

    profile = MultiCheckboxField(
        'Profile',
        validators=[
            DataRequired()
        ],
        choices=[
            ('PROFILE_11', 'PROFILE_11'),
            ('PROFILE_2', 'PROFILE_2'),
            ('PROFILE_3', 'PROFILE_3'),
            ('PROFILE_4', 'PROFILE_4'),
            ('PROFILE_5', 'PROFILE_5')
        ]
    )

    edition = IntegerField(
        'Edition',
        validators=[
            Optional(),
            NumberRange(min=0, max=255, message='Valid range: 0-255 (uint8)')
        ]
    )

    revision = IntegerField(
        'Revision',
        validators=[
            Optional(),
            NumberRange(min=0, max=255, message='Valid range: 0-255 (uint8)')
        ]
    )

    submit = SubmitField('Download')
