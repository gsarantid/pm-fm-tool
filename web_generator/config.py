import os


class Config:

    ''' Flask App Settings '''
    DEBUG = True if os.environ.get('TEST_MODE') else False
    SECRET_KEY = os.environ.get('SECRET_KEY')

