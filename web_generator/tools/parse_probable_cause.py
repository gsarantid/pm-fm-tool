import xmltodict
import json
import os

from flask import current_app

DEFAULT_PROBABLE_CAUSE_OPTION = ('', '--Select Event Type--')


def _parse_probable_causes_xml():
    print(os.getcwd())
    # ----------------------------------------------------
    # TODO: Automate detection of probable-causes.xml file
    probable_causes_path = os.path.join(current_app.root_path, 'tools/probable-causes.xml')
    with open(probable_causes_path) as fd:
    # ----------------------------------------------------
        doc = xmltodict.parse(fd.read())

    event_types = {}
    for pc in doc['probable-causes']['probable-cause']:
        p_label = pc['label']
        p_name = pc['name']
        p_number = pc['number']
        if 'event-type' in pc:
            if isinstance(pc['event-type'], list):
                for event_type in pc['event-type']:
                    if event_type not in event_types:
                        event_types[event_type] = [{p_label: p_name, 'index': p_number}]
                    else:
                        event_types[event_type].append({p_label: p_name, 'index': p_number})
            else:
                if pc['event-type'] not in event_types:
                    event_types[pc['event-type']] = [{p_label: p_name, 'index': p_number}]
                else:
                    event_types[pc['event-type']].append({p_label: p_name, 'index': p_number})

    # Remove Other event type which is considered invalid
    del event_types['other']

    # Generate probable-causes.json
    # with open('probable-causes.json', 'w') as fd:
    #     json.dump(event_types, fd, indent=4)

    return event_types


def get_probable_cause_choices():
    event_types = _parse_probable_causes_xml()
    choices = [
        (k, v)
        for value in event_types.values()
        for pair in value
        for k, v in pair.items()
    ]
    choices.insert(0, DEFAULT_PROBABLE_CAUSE_OPTION)
    return choices


def get_probable_cause_index(probable_cause):
    for event_type in _parse_probable_causes_xml().values():
        for pc in event_type:
            if probable_cause in pc:
                return pc['index']


def print_in_js_format(event_types):
    for event_type, probable_causes in event_types.items():
        size_of_probable_causes = len(probable_causes)
        print('var {} = ['.format(event_type))
        for index, probable_cause in enumerate(probable_causes):
            if index < size_of_probable_causes - 1:
                print('    {{display: "{}", value: "{}"}},'.format(
                    probable_cause[list(probable_cause)[0]],
                    list(probable_cause)[0],
                ))
            else:
                print('    {{display: "{}", value: "{}"}}'.format(
                    probable_cause[list(probable_cause)[0]],
                    list(probable_cause)[0],
                ))
        print('];')
        print('')


if __name__ == '__main__':
    print_in_js_format(_parse_probable_causes_xml())
