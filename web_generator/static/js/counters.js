$(document).ready(function () {

    $('[data-toggle="popover"]').popover();

    $(".counter-group").find(".expand-button").on("click", function() {
        var toggle_elems = $(this).closest(".counter-group").find(".counter-descr-upd, .counter-log-net-time, .counter-orig-stat-trig, .counter-unit-range-cat");
        toggle_elems.toggle();
        if (toggle_elems.is(":hidden")) {
        $(this).find("i").removeClass("fa-caret-down").addClass("fa-caret-right");
        } else {
        $(this).find("i").removeClass("fa-caret-right").addClass("fa-caret-down");
        }
    });

    $("button.add-counter").on("click", function() {       
        var elem = $(this).closest(".counter-group");
        elem.clone(true,true).insertAfter(elem);
        var new_index = elem.index(".counter-group") + 1;

        copy_counter_fields(elem);
        update_counter_fields(new_index);
        $('[data-toggle="popover"]').popover();

    });

    $("button.reset-counter").on("click", function() {
        $(".counter-form").trigger("reset");
    });

     $(".counter-group").on("click", "button.remove-counter", function() {
        var size = $(".counter-section").find(".counter-group").length;
        if (size > 1) {
            $(this).closest(".counter-group").remove();
        }
    });

    function copy_counter_fields(elem) {

        $(elem).find("select").each(function(index) {
            var value = $(this).val();
            var copy_to = $(this).closest(".counter-group").next(".counter-group").find("select").eq(index);
            copy_to.val(value);
        });
    };

    function update_counter_fields(new_index) {
        var upd_counter_group = $(".counter-section").find('.form-group.counter-group').eq(new_index);
        upd_counter_group.find('input, select, textarea').each(function() {

            function getNewFieldNameID(old_name_id, new_index) {
                var oldTokens = old_name_id.split("-");
                return oldTokens[0] + "-" + new_index + "-" + oldTokens[2];
            }

            var newIdName = getNewFieldNameID($(this).attr("id"), new_index);
            $(this).attr("id", newIdName);
            $(this).attr("name", newIdName);
        });
    };
});