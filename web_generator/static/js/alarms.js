$(document).ready(function () {
    // Initialize event-type to probable cause mapping
    var qualityOfServiceAlarm = [
        {display: "Excessive Error Rate", value: "excessiveErrorRate"},
        {display: "Bandwidth Reduction", value: "bandwidthReduction"},
        {display: "Congestion", value: "congestion"},
        {display: "Performance Degraded", value: "performanceDegraded"},
        {display: "Queue Size Exceeded", value: "queueSizeExceeded"},
        {display: "Resource at or Nearing Capacity", value: "resourceAtOrNearingCapacity"},
        {display: "Response Time Excessive", value: "responseTimeExcessive"},
        {display: "Re-transmission Rate Excessive", value: "retransmissionRateExcessive"},
        {display: "Threshold Crossed", value: "thresholdCrossed"},
        {display: "Reduced alarm reporting", value: "reducedAlarmReporting"},
        {display: "Reduced event reporting", value: "reducedEventReporting"},
        {display: "Reduced logging capability", value: "reducedLoggingCapability"},
        {display: "System resources overload", value: "systemResourcesOverload"}
    ];

    var communicationsAlarm = [
        {display: "Alarm Indication Signal", value: "aIS"},
        {display: "Call Setup Failure", value: "callSetupFailure"},
        {display: "Degraded Signal", value: "degradedSignal"},
        {display: "Far End Receiver Failure", value: "farEndReceiverFailure"},
        {display: "Framing Error", value: "framingError"},
        {display: "Loss Of Frame", value: "lossOfFrame"},
        {display: "Loss Of Pointer", value: "lossOfPointer"},
        {display: "Loss Of Signal", value: "lossOfSignal"},
        {display: "PayLoad Type Mismatch", value: "payloadTypeMismatch"},
        {display: "Transmission Error", value: "transmissionError"},
        {display: "Remote Alarm Interface", value: "remoteAlarmInterface"},
        {display: "Excessive Bit Error Rate", value: "excessiveBER"},
        {display: "Path Trace Mismatch", value: "pathTraceMismatch"},
        {display: "Unavailable", value: "unavailable"},
        {display: "Signal Label Mismatch", value: "signalLabelMismatch"},
        {display: "Loss Of MultiFrame", value: "lossOfMultiFrame"},
        {display: "Receive Failure", value: "receiveFailure"},
        {display: "Transmit Failure", value: "transmitFailure"},
        {display: "Modulation Failure", value: "modulationFailure"},
        {display: "Demodulation Failure", value: "demodulationFailure"},
        {display: "Communication Protocol Error", value: "communicationsProtocolError"},
        {display: "Communication Subsystem Failure", value: "communicationsSubsystemFailure"},
        {display: "DTE-DCE Interface Error", value: "dteDceInterfaceError"},
        {display: "LAN Error", value: "lanError"},
        {display: "Local Node Transmission Error", value: "localNodeTransmissionError"},
        {display: "Remote Node Transmission Error", value: "remoteNodeTransmissionError"},
        {display: "Broadcast channel failure", value: "broadcastChannelFailure"},
        {display: "Call establishment error", value: "callEstablishmentError"},
        {display: "Invalid message received", value: "invalidMessageReceived"},
        {display: "Invalid MSU received", value: "invalidMSUReceived"},
        {display: "LAPD link protocol failure", value: "lapdLinkProtocolFailure"},
        {display: "Local alarm indication", value: "localAlarmIndication"},
        {display: "Remote alarm indication", value: "remoteAlarmIndication"},
        {display: "Routing failure", value: "routingFailure"},
        {display: "SS7Protocol failure", value: "ss7ProtocolFailure"},
        {display: "Transmission failure", value: "transmissionFailure"},
        {display: "Connection establishment error", value: "connectionEstablishmentError"}
    ];

    var integrityViolation = [
        {display: "Degraded Signal", value: "degradedSignal"},
        {display: "Framing Error", value: "framingError"},
        {display: "Loss Of Frame", value: "lossOfFrame"},
        {display: "Loss Of Signal", value: "lossOfSignal"},
        {display: "Communication Protocol Error", value: "communicationsProtocolError"},
        {display: "Communication Subsystem Failure", value: "communicationsSubsystemFailure"},
        {display: "DTE-DCE Interface Error", value: "dteDceInterfaceError"},
        {display: "LAN Error", value: "lanError"},
        {display: "Local Node Transmission Error", value: "localNodeTransmissionError"},
        {display: "Remote Node Transmission Error", value: "remoteNodeTransmissionError"},
        {display: "Call establishment error", value: "callEstablishmentError"},
        {display: "Duplicate Information", value: "duplicateInformation"},
        {display: "Information Missing", value: "informationMissing"},
        {display: "Information Modification detected", value: "informationModificationDetected"},
        {display: "Information out of Sequence", value: "informationOutOfSequence"},
        {display: "Unexpected Information", value: "unexpectedInformation"}
    ];

    var operationalViolation = [
        {display: "Denial of Service", value: "denialOfService"},
        {display: "Out of Service", value: "outOfService"},
        {display: "Procedural Error", value: "proceduralError"},
        {display: "Unspecified Reason", value: "unspecifiedReason"}
    ];

    var timeDomainViolation = [
        {display: "Delayed Information", value: "delayedInformation"},
        {display: "Key Expired", value: "keyExpired"},
        {display: "Out of Hours Activity", value: "outOfHoursActivity"}
    ];

    var securityServiceOrMechanismViolation = [
        {display: "Authentication Failure", value: "authenticationFailure"},
        {display: "Breach of Confidentiality", value: "breachOfConfidentiality"},
        {display: "Non Repudiation Failure", value: "nonRepudiationFailure"},
        {display: "Unauthorised Access Attempt", value: "unauthorisedAccessAttempt"},
        {display: "Unspecified Reason", value: "unspecifiedReason"},
        {display: "Unauthorized access attempt", value: "unauthorizedAccessAttempt"}
    ];

    var environmentalAlarm = [
        {display: "Air Compressor Failure", value: "airCompressorFailure"},
        {display: "Air Conditioning Failure", value: "airConditioningFailure"},
        {display: "Air Dryer Failure", value: "airDryerFailure"},
        {display: "Battery Discharging", value: "batteryDischarging"},
        {display: "Battery Failure", value: "batteryFailure"},
        {display: "Commercial Power Failure", value: "commercialPowerFailure"},
        {display: "Cooling Fan Failure", value: "coolingFanFailure"},
        {display: "Engine Failure", value: "engineFailure"},
        {display: "Fire Detector Failure", value: "fireDetectorFailure"},
        {display: "Fuse Failure", value: "fuseFailure"},
        {display: "Generator Failure", value: "generatorFailure"},
        {display: "Low Battery Threshold", value: "lowBatteryThreshold"},
        {display: "Pump Failure", value: "pumpFailure"},
        {display: "Rectifier Failure", value: "rectifierFailure"},
        {display: "Rectifier High Voltage", value: "rectifierHighVoltage"},
        {display: "Rectifier Low Voltage", value: "rectifierLowVoltage"},
        {display: "Ventilations System Failure", value: "ventilationsSystemFailure"},
        {display: "Enclosure Door Open", value: "enclosureDoorOpen"},
        {display: "Explosive Gas", value: "explosiveGas"},
        {display: "Fire", value: "fire"},
        {display: "Flood", value: "flood"},
        {display: "High Humidity", value: "highHumidity"},
        {display: "High Temperature", value: "highTemperature"},
        {display: "High Wind", value: "highWind"},
        {display: "Ice Build Up", value: "iceBuildUp"},
        {display: "Intrusion Detection", value: "intrusionDetection"},
        {display: "Low Fuel", value: "lowFuel"},
        {display: "Low Humidity", value: "lowHumidity"},
        {display: "Low Cable Pressure", value: "lowCablePressure"},
        {display: "Low Temperature", value: "lowTemperature"},
        {display: "Low Water", value: "lowWater"},
        {display: "Smoke", value: "smoke"},
        {display: "Toxic Gas", value: "toxicGas"},
        {display: "External Point Failure", value: "externalPointFailure"},
        {display: "Excessive Vibration", value: "excessiveVibration"},
        {display: "Heating Or Ventilation Or Cooling System Problem", value: "heatingOrVentilationOrCoolingSystemProblem"},
        {display: "Humidity Unacceptable", value: "humidityUnacceptable"},
        {display: "Leak Detection", value: "leakDetection"},
        {display: "Material Supply Exhausted", value: "materialSupplyExhausted"},
        {display: "Pressure Unacceptable", value: "pressureUnacceptable"},
        {display: "Temperature Unacceptable", value: "temperatureUnacceptable"},
        {display: "Toxic Leak Detected", value: "toxicLeakDetected"},
        {display: "Cooling system failure", value: "coolingSystemFailure"},
        {display: "External equipment failure", value: "externalEquipmentFailure"},
        {display: "External power supply failure", value: "externalPowerSupplyFailure"},
        {display: "External transmission device failure", value: "externalTransmissionDeviceFailure"}
    ];

    var physicalViolation = [
        {display: "Intrusion Detection", value: "intrusionDetection"},
        {display: "Cable Tamper", value: "cableTamper"},
        {display: "Unspecified Reason", value: "unspecifiedReason"},
        {display: "Intrusion detected", value: "intrusionDetected"}
    ];

    var equipmentAlarm = [
        {display: "Back Plane Failure", value: "backPlaneFailure"},
        {display: "Data Set Problem", value: "dataSetProblem"},
        {display: "Equipment Identifier Duplication", value: "equipmentIdentifierDuplication"},
        {display: "External IF Device Problem", value: "externalIFDeviceProblem"},
        {display: "Line Card Problem", value: "lineCardProblem"},
        {display: "Multiplexer Problem", value: "multiplexerProblem"},
        {display: "NE Identifier Duplication", value: "nEIdentifierDuplication"},
        {display: "Power Problem", value: "powerProblem"},
        {display: "Processor Problem", value: "processorProblem"},
        {display: "Protection Path Failure", value: "protectionPathFailure"},
        {display: "Replaceable Unit Missing", value: "replaceableUnitMissing"},
        {display: "Replaceable Unit Type Mismatch", value: "replaceableUnitTypeMismatch"},
        {display: "Synchronization Source Mismatch", value: "synchronizationSourceMismatch"},
        {display: "Terminal Problem", value: "terminalProblem"},
        {display: "Timing Problem", value: "timingProblem"},
        {display: "Trunk Card Problem", value: "trunkCardProblem"},
        {display: "Replaceable Unit Problem", value: "replaceableUnitProblem"},
        {display: "Real Time Clock Failure", value: "realTimeClockFailure"},
        {display: "Protection Mechanism Failure", value: "protectionMechanismFailure"},
        {display: "Protection Resource Failure", value: "protectionResourceFailure"},
        {display: "Adapter Error", value: "adapterError"},
        {display: "Data Set Or Modem Error", value: "dataSetOrModemError"},
        {display: "Equipment Malfunction", value: "equipmentMalfunction"},
        {display: "Input/Output Device Error", value: "iODeviceError"},
        {display: "Input Device Error", value: "inputDeviceError"},
        {display: "Output Device Error", value: "outputDeviceError"},
        {display: "Receive Failure", value: "receiveFailure"},
        {display: "Transmit Failure", value: "transmitFailureX733"},
        {display: "A-bis to BTS interface failure", value: "aBisToBTSInterfaceFailure"},
        {display: "A-bis To TRX interface failure", value: "aBisToTRXInterfaceFailure"},
        {display: "Antenna problem", value: "antennaProblem"},
        {display: "Battery breakdown", value: "batteryBreakdown"},
        {display: "Battery charging fault", value: "batteryChargingFault"},
        {display: "Clock synchronisation problem", value: "clockSynchronisationProblem"},
        {display: "Combiner problem", value: "combinerProblem"},
        {display: "Disk problem", value: "diskProblem"},
        {display: "Equipment failure", value: "equipmentFailure"},
        {display: "Excessive receiver temperature", value: "excessiveReceiverTemperature"},
        {display: "Excessive transmitter output power", value: "excessiveTransmitterOutputPower"},
        {display: "Excessive transmitter temperature", value: "excessiveTransmitterTemperature"},
        {display: "Frequency hopping degraded", value: "frequencyHoppingDegraded"},
        {display: "Frequency hopping failure", value: "frequencyHoppingFailure"},
        {display: "Frequency redefinition failed", value: "frequencyRedefinitionFailed"},
        {display: "Line interface failure", value: "lineInterfaceFailure"},
        {display: "Link failure", value: "linkFailure"},
        {display: "Loss of synchronisation", value: "lossOfSynchronisation"},
        {display: "Lost redundancy", value: "lostRedundancy"},
        {display: "Mains breakdown with battery backup", value: "mainsBreakdownWithBatteryBackup"},
        {display: "Mains breakdown without battery backup", value: "mainsBreakdownWithoutBatteryBackup"},
        {display: "Power supply failure", value: "powerSupplyFailure"},
        {display: "Receiver antenna fault", value: "receiverAntennaFault"},
        {display: "Receiver Failure", value: "receiverFailure"},
        {display: "Receiver multi-coupler failure", value: "receiverMultiCouplerFailure"},
        {display: "Reduced transmitter output power", value: "reducedTransmitterOutputPower"},
        {display: "Signal quality evaluation fault", value: "signalQualityEvaluationFault"},
        {display: "Timeslot hardware failure", value: "timeslotHardwareFailure"},
        {display: "Transceiver problem", value: "transceiverProblem"},
        {display: "Transcoder problem", value: "transcoderProblem"},
        {display: "Transcoder or rate adapter problem", value: "transcoderOrRateAdapterProblem"},
        {display: "transmitter antenna failure", value: "transmitterAntennaFailure"},
        {display: "Transmitter antenna not adjusted", value: "transmitterAntennaNotAdjusted"},
        {display: "Transmitter failure", value: "transmitterFailure"},
        {display: "Transmitter low voltage or current", value: "transmitterLowVoltageOrCurrent"},
        {display: "Transmitter off frequency", value: "transmitterOffFrequency"}
    ];

    var processingErrorAlarm = [
        {display: "Storage Capacity Problem", value: "storageCapacityProblem"},
        {display: "Memory Mismatch", value: "memoryMismatch"},
        {display: "Corrupt Data", value: "corruptData"},
        {display: "Out Of CPU Cycles", value: "outOfCPUCycles"},
        {display: "Software Environment Problem", value: "sfwrEnvironmentProblem"},
        {display: "Software Download Failure", value: "sfwrDownloadFailure"},
        {display: "Loss Of Real Time", value: "lossOfRealTimel"},
        {display: "ReInitialized", value: "reInitialized"},
        {display: "Application Subsystem Failure", value: "applicationSubsystemFailure"},
        {display: "Configuration Or Customization Error", value: "configurationOrCustomizationError"},
        {display: "CPU Cycles Limit Exceeded", value: "cpuCyclesLimitExceeded"},
        {display: "File Error", value: "fileError"},
        {display: "Out Of Memory", value: "outOfMemory"},
        {display: "Software Error", value: "softwareError"},
        {display: "Software Program Abnormally Terminated", value: "softwareProgramAbnormallyTerminated"},
        {display: "Software Program Error", value: "softwareProgramError"},
        {display: "Underlying Resource Unavailable", value: "underlyingResourceUnavailable"},
        {display: "Version Mismatch", value: "versionMismatch"},
        {display: "Database inconsistency", value: "databaseInconsistency"},
        {display: "File system call unsuccessful", value: "fileSystemCallUnsuccessful"},
        {display: "Input parameter out of range", value: "inputParameterOutOfRange"},
        {display: "Invalid parameter", value: "invalidParameter"},
        {display: "Invalid pointer", value: "invalidPointer"},
        {display: "Message not expected", value: "messageNotExpected"},
        {display: "Message not initialised", value: "messageNotInitialised"},
        {display: "Message out of sequence", value: "messageOutOfSequence"},
        {display: "System call unsuccessful", value: "systemCallUnsuccessful"},
        {display: "Time out expired", value: "timeOutExpired"},
        {display: "Variable out of range", value: "variableOutOfRange"},
        {display: "Watch dog timer expired", value: "watchDogTimerExpired"}
    ];

    var headerFields = [
        ['service', 'For applications it is based on pod name, for infra components the log shipper can add this', 'string'],
        ['system', 'Name of the TAS, given by the operator', 'string'],
        ['host', 'The name of the host', 'string'],
        ['process', 'Name and/or ID of the process', 'string'],
        ['container', 'The 12 digit (short) ID of the container', 'hex'],
        ['level', 'Log level according to the kernel log levels', 'string/enum'],
        ['facility', 'Used to distinguish log messages needing special treatment', 'string'],
        ['neid', 'Unique ID of the TAS, assigned by Nokia', 'integer'],
        ['time', 'UTC timestamp in internet date/time format according to RFC3339', 'string'],
        ['type', 'Type of the log entry', 'string/enum'],
        ['timezone', 'Local timezone of the host', 'string']
    ];

    var serviceHeaderField = [
        ['service', 'For applications it is based on pod name, for infra components the log shipper can add this', 'string']
    ]

    // Enable popover
    $('[data-toggle="popover"]').popover()

    // Initialize Alive Time value
    if ($("#alive_time").val() != "SEC_TILL_CANCEL") {
        $("#alive_time_value").val('-- Alive Time--');
        $("#alive_time_value").prop('disabled', true);
    }

    // Enable Alive Time in seconds in case of SEC_TILL_CANCEL
    $("#alive_time").change(function () {
        if ($(this).val() == 'SEC_TILL_CANCEL') {
            $("#alive_time_value").val('');
            $("#alive_time_value").prop('disabled', false);
        } else {
            $("#alive_time_value").val('--Alive Time--');
            $("#alive_time_value").prop('disabled', true);
        }
    });

    var probable_cause = $("#probable_cause option:selected").val();
    var event_type = $("#event_type").val();
    switch (event_type) {
        case "communicationsAlarm":
            get_probable_causes_on_exception(communicationsAlarm, probable_cause);
            break;
        case "integrityViolation":
            get_probable_causes_on_exception(integrityViolation, probable_cause);
            break;
        case "equipmentAlarm":
            get_probable_causes_on_exception(equipmentAlarm, probable_cause);
            break;
        case "environmentalAlarm":
            get_probable_causes_on_exception(environmentalAlarm, probable_cause);
            break;
        case "physicalViolation":
            get_probable_causes_on_exception(physicalViolation, probable_cause);
            break;
        case "processingErrorAlarm":
            get_probable_causes_on_exception(processingErrorAlarm, probable_cause);
            break;
        case "qualityOfServiceAlarm":
            get_probable_causes_on_exception(qualityOfServiceAlarm, probable_cause);
            break;
        case "securityServiceOrMechanismViolation":
            get_probable_causes_on_exception(securityServiceOrMechanismViolation, probable_cause);
            break;
        case "timeDomainViolation":
            get_probable_causes_on_exception(timeDomainViolation, probable_cause);
            break;
        case "operationalViolation":
            get_probable_causes_on_exception(operationalViolation, probable_cause);
            break;
        default:
            $("#probable_cause").empty();
            $("#probable_cause").append("<option>--Select Event Type--</option>");
            $("#probable_cause").prop('disabled', true);
        break;
    }

    // Adjust probable cause value based on event type value
    $("#event_type").change(function () {
        // switch_probable_causes();
        var event_type = $(this).val();
        switch (event_type) {
            case "communicationsAlarm":
                get_probable_causes(communicationsAlarm);
                break;
            case "integrityViolation":
                get_probable_causes(integrityViolation);
                break;
            case "equipmentAlarm":
                get_probable_causes(equipmentAlarm);
                break;
            case "environmentalAlarm":
                get_probable_causes(environmentalAlarm);
                break;
            case "physicalViolation":
                get_probable_causes(physicalViolation);
                break;
            case "processingErrorAlarm":
                get_probable_causes(processingErrorAlarm);
                break;
            case "qualityOfServiceAlarm":
                get_probable_causes(qualityOfServiceAlarm);
                break;
            case "securityServiceOrMechanismViolation":
                get_probable_causes(securityServiceOrMechanismViolation);
                break;
            case "timeDomainViolation":
                get_probable_causes(timeDomainViolation);
                break;
            case "operationalViolation":
                get_probable_causes(operationalViolation);
                break;
            default:
                $("#probable_cause").empty();
                $("#probable_cause").append("<option>--Select Event Type--</option>");
                $("#probable_cause").prop('disabled', true);
            break;
        }
    });

    // Handle alarm fields change value
    load_alarm_fields_event_handler();

    $("button.reset-all").on("click", function () {
        $(".alarm-form").trigger("reset");
    });

    // Add alarm field
    $(".add-alarm-field").click(function() {
        var html = $(".copy-alarm-field").html();
        $(".copy-alarm-field").before(html);
        update_alarm_fields();

        // Load event handler for every new alarm field
        load_alarm_fields_event_handler()

        if ($('#compare_fields').val() == "ALL") {
            $(".table-compared-fields").empty();
            $(".table-compared-fields").append(createUserDefinedFieldsTable(true));
            $(".table-compared-fields").append(createHeaderFieldsTable(serviceHeaderField));
        } else if ($('#compare_fields').val() == "LIST") {
            var new_user_alarm_field = createNewUserDefinedField(0, false, '', '', '');
            $('table.user-defined-fields').find('tbody').append(createCsrfToken(0));
            $('table.user-defined-fields').find('tbody').append(new_user_alarm_field);
            indexUserDefinedFields();
        }
    });

    // Remove alarm field
    $("body").on("click", ".remove-alarm-field", function() {
        var deleted_id = $(this).parent().parent().first().find('input').attr('id').split('-')[1];

        $(this).parents(".control-group").remove();
        update_alarm_fields();

        // Handle User-Defined Fields table
        if ($('#compare_fields').val() == "ALL") {
            $(".table-compared-fields").empty();
            $(".table-compared-fields").append(createUserDefinedFieldsTable(true));
        } else if ($('#compare_fields').val() == "LIST") {
            $("#compare_fields_list-" + deleted_id + "-csrf_token").remove();
            $("#compare_fields_list-" + deleted_id + "-compare_field_name").parent().parent("tr").remove();
            indexUserDefinedFields();
        }
    });

    // Handle compare fields form change
    $("#compare_fields").change(function () {
        $(".table-compared-fields").empty();
        if ($(this).val() == "ALL") {
            $(".table-compared-fields").append(createUserDefinedFieldsTable(true));
            $(".table-compared-fields").append(createHeaderFieldsTable(serviceHeaderField));
        } else if ($(this).val() == "LIST") {
            $(".table-compared-fields").append(createUserDefinedFieldsTable(false));
            $(".table-compared-fields").append(createHeaderFieldsTable(headerFields));
        } else {
            $(".table-compared-fields").append(createHeaderFieldsTable(serviceHeaderField));
        }
    });

    
    function load_alarm_fields_event_handler() {
        $('.form-control.alarm_field_name').change(function () {
            var alarm_field_name_id = $(this).attr('id').split('-')[1];
            var alarm_field_name = $(this).val();
            $('#compare_fields_list-' + alarm_field_name_id + '-compare_field_name').val(alarm_field_name);
        });

        $('.form-control.alarm_field_description').change(function () {
            var alarm_field_description_id = $(this).attr('id').split('-')[1];
            var alarm_field_description = $(this).val();
            $('#compare_fields_list-' + alarm_field_description_id + '-compare_field_description').val(alarm_field_description);
        });

        $('.form-control.alarm_field_type').change(function () {
            var alarm_field_type_id = $(this).attr('id').split('-')[1];
            var alarm_field_type = $(this).val();
            $('#compare_fields_list-' + alarm_field_type_id + '-compare_field_type').val(alarm_field_type);
        });
    };

    function indexUserDefinedFields() {
        $('table.user-defined-fields').find('tbody').find('tr').each(function(index) {

            // Update compare field checkbox
            $(this).find('input[type="checkbox"]').attr('id', 'compare_fields_list-' + index + '-is_comparable');
            $(this).find('input[type="checkbox"]').attr('name', 'compare_fields_list-' + index + '-is_comparable');
            $(this).find('input[type="checkbox"]').attr('class', 'form-check-input compare_fields_list-' + index + '-is_comparable');

            // Update compare field name
            $(this).find('input.compare_field_name').attr("id", "compare_fields_list-" + index + "-compare_field_name");
            $(this).find('input.compare_field_name').attr('name', "compare_fields_list-" + index + "-compare_field_name");

            // Update compare field description
            $(this).find('textarea.compare_field_description').attr("id", "compare_fields_list-" + index + "-compare_field_description");
            $(this).find('textarea.compare_field_description').attr("name", "compare_fields_list-" + index + "-compare_field_description");

            // Update compare field type
            $(this).find('input.compare_field_type').attr("id", "compare_fields_list-" + index + "-compare_field_type");
            $(this).find('input.compare_field_type').attr("name", "compare_fields_list-" + index + "-compare_field_type");
        });

        $('table.user-defined-fields').find('tbody').find('input[type="hidden"]').each(function(index) {
            $(this).attr('id', 'compare_fields_list-' + index + '-csrf_token');
            $(this).attr('name', 'compare_fields_list-' + index + '-csrf_token');
        });
    };

    function createUserDefinedFieldsTable(isComparable) {

        var table = $('<table class="table table-hover user-defined-fields"></table>');
        table.append('<caption style="caption-side:top">User-Defined Fields</caption>');
        var thead = $('<thead></thead>');
        var tbody = $('<tbody></tbody>');
        var tr = $('<tr></tr>');
        var th = $('<th></th>');

        // Construct thead tag
        var head_row = tr.clone();
        head_row.append(th.clone().text('Comparable'));
        head_row.append(th.clone().text('Field Name'));
        head_row.append(th.clone().text('Field Description'));
        head_row.append(th.clone().text('Field Type'));
        thead.append(head_row);
        table.append(thead);

        // Construct tbody tag
        $(".alarm-fields").find('.input-group.control-group.alarm-field').each(function(index) {
            var name = $('#alarm_fields-' + index + '-alarm_field_name').val();
            var description = $('#alarm_fields-' + index + '-alarm_field_description').val();
            var type = $('#alarm_fields-' + index + '-alarm_field_type').val();

            var body_row = createNewUserDefinedField(index, isComparable, name, description, type);

            tbody.append(createCsrfToken(index));
            tbody.append(body_row);
            table.append(tbody);
        });
        return table;
    };

    function createNewUserDefinedField(index, isComparable, name, description, type) {
        var tr = $('<tr></tr>');
        var td = $('<td></td>');
        var checkbox = createCheckBox(index, isComparable);
        var compare_field_name = createCompareFieldName(index, name);
        var compare_field_description = createCompareFieldDescription(index, description);
        var compare_field_type = createCompareFieldType(index, type);

        var body_row = tr.clone();
        if (isComparable == true) {
            body_row.append(td.clone().attr('style','padding-left:5%').append(checkbox.attr('checked', true).prop('disabled', true)));
        } else {
            body_row.append(td.clone().attr('style','padding-left:5%').append(checkbox.attr('checked', false)));
        }
        body_row.append(td.clone().append(compare_field_name));
        body_row.append(td.clone().append(compare_field_description));
        body_row.append(td.clone().append(compare_field_type));
        return body_row;
    };

    function noValidAlarmFields() {
        var res = true;
        $(".alarm-fields").find('.input-group.control-group.alarm-field').each(function(index) {
            var name = $('#alarm_fields-' + index + '-alarm_field_name').val();
            var description = $('#alarm_fields-' + index + '-alarm_field_description').val();
            var type = $('#alarm_fields-' + index + '-alarm_field_type').val();
            if (name != '' || description != '' || type != '') {
                res = false;
                return;
            }
        });
        return res;
    };

    function createCheckBox(index, isComparable) {
        var id = "compare_fields_list-" + index + "-is_comparable";
        if (isComparable == true) {
            var input = $('<input></input>').addClass('form-check-input ' + id).attr('type', 'checkbox').attr('id', id).attr('name', id).attr('checked', true).prop('disabled', true);
        } else {
            var input = $('<input></input>').addClass('form-check-input ' + id).attr('type', 'checkbox').attr('id', id).attr('name', id);
        }
        return input
    };

    function createCsrfToken(index) {
        var csrf_value = $('#csrf_token').first().val();
        var id = "compare_fields_list-" + index + "-csrf_token";
        var input = $('<input></input>').attr('id', id).attr('name', id).attr('type', 'hidden').attr('value', csrf_value);
        return input;
    };

    function createCompareFieldName(index, name) {
        var id = "compare_fields_list-" + index + "-compare_field_name";
        var input = $('<input></input>').addClass('form-control compare_field_name').attr('id', id).attr('name', id).attr('type', 'text').prop('readonly', true).attr('value', name);
        return input;
    };

    function createCompareFieldDescription(index, description) {
        var id = "compare_fields_list-" + index + "-compare_field_description";
        var input = $('<textarea></textarea>').addClass('form-control compare_field_description').attr('id', id).attr('name', id).attr('type', 'text').prop('readonly', true).text(description);
        return input;
    };

    function createCompareFieldType(index, type) {
        var id = "compare_fields_list-" + index + "-compare_field_type";
        var input = $('<input></input>').addClass('form-control compare_field_type').attr('id', id).attr('name', id).attr('type', 'text').prop('readonly', true).attr('value', type);
        return input;
    };

    function createHeaderFieldsTable(header_fields) {
        var num_of_alarm_fields = $(".alarm-fields").find('.input-group.control-group.alarm-field').length;

        var table = $('<table class="table table-hover header-fields"></table>');
        table.append('<caption style="caption-side:top">Header Fields</caption>');
        var thead = $('<thead></thead>');
        var tbody = $('<tbody></tbody>');
        var tr = $('<tr></tr>');
        var th = $('<th></th>');
        var td = $('<td></td>');
        var checkbox = $('<input type="checkbox">');

        // Construct thead tag
        var head_row = tr.clone();
        head_row.append(th.clone().text('Comparable'));
        head_row.append(th.clone().text('Field Name'));
        head_row.append(th.clone().text('Field Description'));
        head_row.append(th.clone().text('Field Type'));
        thead.append(head_row);
        table.append(thead);

        $(header_fields).each(function(index) {
            header_index = num_of_alarm_fields + index;
            var name = header_fields[index][0];
            var description = header_fields[index][1];
            var type = header_fields[index][2];

            if (name == 'service') {
                var checkbox = createHeaderCheckBox(header_index, true, true);
            } else {
                var checkbox = createHeaderCheckBox(header_index, false, false);
            }
            var compare_field_name = createHeaderFieldName(header_index, name);
            var compare_field_description = createHeaderFieldDescription(header_index, description);
            var compare_field_type = createHeaderFieldType(header_index, type);

            var body_row = tr.clone();
            body_row.append(td.clone().attr('style','padding-left:5%').append(checkbox));
            body_row.append(td.clone().append(compare_field_name));
            body_row.append(td.clone().append(compare_field_description));
            body_row.append(td.clone().append(compare_field_type));

            tbody.append(createHeaderCsrfToken(header_index));
            tbody.append(body_row);
            table.append(tbody);
        });

        return table;
    };

    function createHeaderCheckBox(index, isComparable, isDisabled) {
        var id = "header_fields_list-" + index + "-is_comparable";
        var input = $('<input></input>').addClass('form-check-input ' + id).attr('type', 'checkbox').attr('id', id).attr('name', id)

        if (isComparable == true) {
            input = input.attr('checked', true);
        }

        if (isDisabled == true) {
            input = input.prop('disabled', true);
        }

        return input
    };

    function createHeaderCsrfToken(index) {
        var csrf_value = $('#csrf_token').first().val();
        var id = "header_fields_list-" + index + "-csrf_token";
        var input = $('<input></input>').attr('id', id).attr('name', id).attr('type', 'hidden').attr('value', csrf_value);
        return input;
    };

    function createHeaderFieldName(index, name) {
        var id = "header_fields_list-" + index + "-header_field_name";
        var input = $('<input></input>').addClass('form-control header_field_name').attr('id', id).attr('name', id).attr('type', 'text').prop('readonly', true).attr('value', name);
        return input;
    };

    function createHeaderFieldDescription(index, description) {
        var id = "header_fields_list-" + index + "-header_field_description";
        var input = $('<textarea></textarea>').addClass('form-control input-lg header_field_description').attr('id', id).attr('name', id).attr('type', 'text').prop('readonly', true).text(description);
        return input;
    };

    function createHeaderFieldType(index, type) {
        var id = "header_fields_list-" + index + "-header_field_type";
        var input = $('<input></input>').addClass('form-control header_field_type').attr('id', id).attr('name', id).attr('type', 'text').prop('readonly', true).attr('value', type);
        return input;
    };

    function update_alarm_fields() {
        var num_of_alarm_fields = $(".alarm-fields").find('.input-group.control-group.alarm-field').length
        $(".alarm-fields").find('.input-group.control-group.alarm-field').each(function(index) {
            if (index < num_of_alarm_fields-1) {
                $(this).find('input').first().attr("id", "alarm_fields-" + (index+1) + "-csrf_token")
                $(this).find('input').first().attr("name", "alarm_fields-" + (index+1) + "-csrf_token")

                $(this).find('input.alarm_field_name').attr("id", "alarm_fields-" + (index+1) + "-alarm_field_name");
                $(this).find('input.alarm_field_name').attr('name', $(this).find('input.alarm_field_name').attr('id'));

                $(this).find('textarea.alarm_field_description').attr("id", "alarm_fields-" + (index+1) + "-alarm_field_description");
                $(this).find('textarea.alarm_field_description').attr('name', $(this).find('textarea.alarm_field_description').attr('id'));

                $(this).find('input.alarm_field_type').attr("id", "alarm_fields-" + (index+1) + "-alarm_field_type");
                $(this).find('input.alarm_field_type').attr('name', $(this).find('input.alarm_field_type').attr('id'));
            }
        });
    };

    function get_probable_causes(event_type) {
        $("#probable_cause").empty();
        $("#probable_cause").append("<option>--Select--</option>");
        $(event_type).each(function(i) {
            $("#probable_cause").append("<option value=\"" + event_type[i].value + "\">" + event_type[i].display + "</option>");
        });
        $("#probable_cause").prop('disabled', false);
    };

    function get_probable_causes_on_exception(event_type, selected_probable_cause) {
        $("#probable_cause").empty();
        $("#probable_cause").append("<option>--Select--</option>");
        $(event_type).each(function(i) {
            if (event_type[i].value == selected_probable_cause) {
                $("#probable_cause").append("<option selected=\"\" value=\"" + event_type[i].value + "\">" + event_type[i].display + "</option>");
            } else {
                $("#probable_cause").append("<option value=\"" + event_type[i].value + "\">" + event_type[i].display + "</option>");
            }
        });
        $("#probable_cause").prop('disabled', false);
    };
});
