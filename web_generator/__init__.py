from flask import Flask
from web_generator.config import Config


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)

    from web_generator.main.routes import main
    from web_generator.alarms.routes import alarms
    from web_generator.measurements.routes import measurements
    from web_generator.errors.handlers import errors

    app.register_blueprint(main)
    app.register_blueprint(alarms)
    app.register_blueprint(measurements)
    app.register_blueprint(errors)

    return app

