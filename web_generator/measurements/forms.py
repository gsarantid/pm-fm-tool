import re,sys
# sys.path.append('useful_methods')
# from .useful_methods import convert_to_camel_case
from flask_wtf import FlaskForm

from wtforms import (StringField, SubmitField, TextAreaField, IntegerField, SelectField,
	FieldList, FormField, BooleanField, SelectMultipleField,widgets)
from wtforms.validators import DataRequired, Regexp, Length,ValidationError, Optional


class MeasurementsFieldForm(FlaskForm):
	measurements_field_name = StringField(
		'Field name',
		validators=[
			DataRequired()
			],
		render_kw={'placeholder': 'Field name'}
	)

	measurement_field_type = StringField(
		'Field type',
		render_kw={'placeholder': 'Field type'}
	)


def newline():
	message = "Must make of use '\n'."
	slashn = "\r\n" #gia windows \r\n , linux \n
	def _newline(form, field):
		if slashn in field.data:
			result = re.sub(slashn, "\\\\n", field.data)
			field.data = result
	return _newline


def replaceSingleQuote():
	message = "Single quote?"
	singlequote = "'"
	def _replaceSingleQuote(form,field):
		if singlequote in field.data:
			result = re.sub(singlequote, "'", field.data)

			field.data = result
			#raise ValidationError(field.data)
	return _replaceSingleQuote

class CountersForm(FlaskForm):

	counter_field_name = StringField(
		'Counter name',
		validators= [DataRequired(),
					 Regexp(r'[A-Z0-9]',message='Counter field name should be in capital alphanumeric without underscores'),
					 Length(min=1, max=28, message="Up to 28 characters allowed.")
				],
		render_kw={'placeholder': 'Counter name'}
	)

	counter_field_counterid = StringField(
		'CounterID',
		validators= [
			DataRequired(),
			Regexp(r'(([A-Z0-9]+))',message='Only capital alphanumerical allowed.'),
			Length(min=3, max= 10)
			],
		render_kw={'placeholder': 'CounterID'}
	)

	counter_field_objectdn = StringField(
		'ObjectDN',
		validators= [
			DataRequired(),
			Regexp(r'([a-z]|/)|([A-Z]|/)',message='Should contain capital and/or lowercase alphanumeric and slashes /')
		],
		render_kw={'placeholder':'ObjectDN'}
	)

	counter_field_description = TextAreaField(
		'Description',
		validators= [
			DataRequired(),
			Regexp(r'([A-Z]?[a-z0-9])'),
			newline()
		],
		render_kw={'placeholder':'Description'}
	)

	counter_field_logical_type = SelectField(
		'Logical_type',
		choices= [
			(None, '--Select--'),
			('sum','sum'),
			('current','current'),
			('max','max'),
			('min','min'),
			('avg','avg')
		]
	)

	counter_field_net_aggregation = SelectField(
		'Net_aggregation',

		choices= [
			(None, '--Select--'),
			('sum','sum'),
			('avg','avg'),
			('min','min'),
			('max','max')
		]
	)

	counter_field_time_aggregation = SelectField(
		'Time_aggregation',
		choices = [
			(None, '--Select--'),
			("sum",'sum'),
			("avg",'avg'),
			("min",'min'),
			("max",'max')
		]
	)

	counter_field_unit = SelectField(
		'Unit',
		choices= [
			(None, '--Select--'),
			("%", "%"),
			("%/min", "%min"),
			("bytes","bytes"),
			("Byte/s","Byte/s"),
                        ('bytes_per_second', 'bytes_per_second'),
			("Decimal number", "Decimal number"),
			("Events/s", "Events/s"),
			("erlangs", "erlangs"),
			("faults/sec", "faults/sec"),
			("integer_number", "integer_number"),
                        ('KB', 'KB'),
			("MB","MB"),
			("minutes", "minutes"),
			("millicores","millicores"),
			("Milliseconds","Milliseconds"),
                        ('ms', 'ms'),
                        ('Operations/s', 'Operations/s'),
			("nanoseconds","nanoseconds"),
                        ('percentage', 'percentage'),
			("seconds", "seconds"),

		]
	)

	counter_field_range = SelectField(
		'Range',
		choices= [
			(None,'--Select--'),
			('0-100','0-100'),
			('0-4294967294','0-4294967294'),
			('0-9223372036854775807','0-9223372036854775807')
		]
	)

	counter_field_categories = SelectField(
		'Categories',
		choices= [
			(None,'--Select--'),
			('Customer','Customer'),
			('R&D','R&D'),
			('Removed','Removed'),
			('CuDo','CuDo'),
			('NetAct','NetAct')
		]
	)

	counter_field_original_release = SelectField(
		'Original_release',
		choices = [
			(None, '--Select--'),
			('Nokia TAS 20.2', 'Nokia TAS 20.2'),
                        ('Nokia TAS 20.5', 'Nokia TAS 20.5'),
                        ('Nokia TAS 20.8', 'Nokia TAS 20.8'),
                        ('Nokia TAS 21.0', 'Nokia TAS 21.0'),
                        ('Nokia TAS 21.2', 'Nokia TAS 21.2'),
                        ('Nokia TAS 21.5', 'Nokia TAS 21.5'),
                        ('Nokia TAS 21.8', 'Nokia TAS 21.8')
		]
	)

	counter_field_status = SelectField(
		'Status',
		choices= [
			(None, '--Select--'),
			('Insert','Insert'),
			('update','update')]
	)

	counter_field_trigger_type = SelectField(
		'Trigger_type',
		choices= [
			(None,'--Select--'),
			('Duration','Duration'),
			('Event','Event'),
			('Sample','Sample')
		]
	)

	counter_field_updated = TextAreaField(
		'Updated',
		validators=[
			Optional()
		],
		render_kw={'placeholder': 'Updated'}
	)

	# counter_field_vesname = convert_to_camel_case(counter_field_name)


class MeasurementForm(FlaskForm):
	measurement_name = StringField(
		'Measurement name',
		validators= [DataRequired(),
					 Regexp(r'^([A-Z0-9])'),
					 Length(min=1, max=4, message='Up to 4 characters allowed.')
					 ]
	)

	measurement_presentation = StringField(
		'Presentation',
		validators=[
			DataRequired(),
			Regexp(r'[A-Z]|[a-z]+|[0-9]', message='First letter of the first word should be capitalized. Any acronyms are ok.')
			]
	)

	measurement_id = StringField(
		'Measurement ID',
		validators = [
			DataRequired(),
			Regexp(r'[0-9]',message='The display ID of the measurement. Sample: 131'),
			Length(min=1,max=5,message='Up to 6 integers allowed.')
		]
	)

	measurement_description = TextAreaField(
		'Measurement Description',
		validators= [
			DataRequired(),
			Regexp(r"([A-Za-z!@#$%^&*].+)",
				   message='Only capital alphanumeric and/or lowercase alphanumeric allowed'),
			newline(),
			replaceSingleQuote()
		]
	)

	measurement_network_profile = StringField(
		'Network profile',
		validators = [
			DataRequired(),
			Regexp(r'[A-Z]',message='Only capital letters allowed')
		]
	)

	measurement_profile = SelectMultipleField(
		'Profile',
		choices = [
                        ('bcs', 'bcs'),
                        ('mcs','mcs'),
                        ('scc', 'scc'),
                        ('smsf', 'smsf'),
                        ('igw', 'igw')
		]
	)

	measurement_category = SelectField(
		'Category',
		choices = [
			('category_observations','Category observations'),
			('database_measurements', 'Database measurements'),
			('protocol_interface_measurements', 'Protocol interface measurements'),
			('resource_usage_measurements', 'Resource usage measurements'),
			('see_measurements', 'See measurements'),
			('signaling_measurements', 'Signaling measurements'),
			('subscriber_db_measurements', 'Subscriber db measurements'),
			('swam_report', 'Swam report'),
			('system_measurements', 'System measurements'),
			('traffic_measurements', 'Traffic measurements')
		]
	)

	counter_fields = FieldList(FormField(CountersForm), min_entries=1)

	submit = SubmitField('Download')



