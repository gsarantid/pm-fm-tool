from flask import render_template, Blueprint, request, current_app, send_file, flash, jsonify
import json
import os

from web_generator.measurements.forms import MeasurementForm

measurements = Blueprint('measurements', __name__)


@measurements.route("/measurements/new", methods=['GET', 'POST'])
def new_measurement():
    form = MeasurementForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            new_measurement = render_template('measurements_definitions_template.html', form=form)

            filename = '{}_MeasType.json'.format(form.measurement_id.data)

            file_path = os.path.join(current_app.root_path, 'tools/new_measurements/',filename)
            os.makedirs(os.path.dirname(file_path), exist_ok=True)

            with open(file_path, 'w') as fm:
                fm.write(new_measurement)

            return send_file(
                file_path,
                mimetype='application/json',
                attachment_filename=filename,
                as_attachment=True,
            )
        else:
            flash('Validation failed!','danger')
    return render_template('create_measurement.html', title='New Measurement',form=form, legend='New Measurement')



