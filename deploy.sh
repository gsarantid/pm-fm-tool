#!/bin/bash

SERVER="localhost"
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


function build_image() {
    isAlreadyPresentImage=$(docker images web-generator | grep -w ${LATEST_HASH})
    if [[ -z ${isAlreadyPresentImage} ]]; then
        docker build -t ${IMAGE_NAME} .
    else
        echo "Image \"${IMAGE_NAME}\" is already present"
    fi
}

function deploy() {
    LATEST_HASH=$(git rev-parse --short HEAD)
    IMAGE_NAME="web-generator:${LATEST_HASH}"

    isAlreadyPresentContainer=$(docker ps -f name=${CONTAINER_NAME} | grep -w ${IMAGE_NAME})
    if [[ ! -z ${isAlreadyPresentContainer} ]]; then
        # Exit in case the container is already running
        echo "Container ${CONTAINER_NAME} is already running"
        exit
    fi

    build_image

    # Deploy dockerized web-tool
    if [ "${mode}" == "prod" ]; then
        docker run -d -p ${PORT}:8085 --rm --name ${CONTAINER_NAME} ${IMAGE_NAME} > /dev/null
    else
        docker run -d -e TEST_MODE='true' -p ${PORT}:8085 --rm -v ${CURRENT_DIR}:/app --name ${CONTAINER_NAME} ${IMAGE_NAME} > /dev/null
    fi

    if [ $? == 0 ]; then
        echo -e "\n${CONTAINER_NAME} has been deployed successfully!"
        echo -e "Access at: http://${SERVER}:${PORT}"
    else
        echo -e "\n${CONTAINER_NAME} failed to be deployed"
    fi
}

function resolveMode() {
    if [[ -n "${mode}" ]]; then
        if [ "${mode}" == "test" ]; then
            CONTAINER_NAME="web-generator-test"
            PORT="5001"
        elif [ "${mode}" == "prod" ]; then
            CONTAINER_NAME="web-generator-prod"
            PORT="5000"
        else
            echo "Invalid mode. Please choose between 'test' or 'prod' mode"
            exit
        fi
    else
        echo "Please choose between 'test' or 'prod' mode"
        exit
    fi
}

mode=$1

resolveMode
deploy
